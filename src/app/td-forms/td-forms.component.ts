import { Component } from '@angular/core';
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-td-forms',
  templateUrl: './td-forms.component.html',
  styleUrls: ['./td-forms.component.css']
})
export class TdFormsComponent {
  plans: {value: string, text: string}[] = [
    {value: 'BASIC', text: 'Basic'},
    {value: 'ADVANCED', text: 'Advanced'},
    {value: 'PRO', text: 'Pro'},
  ];
  plan: string = 'ADVANCED';
  user!: { email: string, plan: string, password: string; };
  wasValidated: boolean = false;
  isValid: boolean = false;

  submitForm(form: NgForm) {
    console.log(form);
    this.wasValidated = true;
    this.isValid = form.touched && form.submitted && form.valid ? form.valid : false;
    if (!this.isValid) {
      return;
    }
    this.user = {
      email: form.value.email,
      plan: form.value.plan,
      password: form.value.password,
    };
    console.log(this.user)
  }
}
