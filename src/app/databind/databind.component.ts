import { Component } from '@angular/core';

@Component({
  selector: 'app-data-bind',
  templateUrl: './databind.component.html',
  styleUrls: ['./databind.component.css']
})
export class DatabindComponent {
  username: string = '';

  resetUsername() {
    this.username = '';
  }
}
