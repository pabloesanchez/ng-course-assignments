import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserCounterService {
  activeToInactive: number = 0;
  inactiveToActive: number = 0;

  constructor() { }

  increaseActiveToInactive() {
    this.activeToInactive++;
    console.log("ACTIVE to INACTIVE count is " + this.activeToInactive);
  }
  increaseInactiveToActive() {
    this.inactiveToActive++;
    console.log("INACTIVE to ACTIVE count is " + this.inactiveToActive);
  }
}
