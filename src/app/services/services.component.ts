import {Component} from '@angular/core';
import {UserService} from "./user.service";

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css'],
})
export class ServicesComponent {
  activeUsers: {name: string, status: string }[]  = [];
  inactiveUsers: {name: string, status: string }[]  = [];


  constructor(private userService: UserService) {
    this.activeUsers = userService.activeUsers;
    this.inactiveUsers = userService.inactiveUsers;
  }
}
