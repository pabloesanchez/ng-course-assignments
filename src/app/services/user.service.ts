import { Injectable } from '@angular/core';
import {UserCounterService} from "./user-counter.service";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public activeUsers: { name: string; status: string }[] = [
    {name: 'Sypha', status: 'ACTIVE'},
    {name: 'Trevor', status: 'ACTIVE'},
    {name: 'Alucard', status: 'ACTIVE'},
  ];
  public inactiveUsers: { name: string; status: string }[] = [
    {name: 'Dracula', status: 'INACTIVE'},
    {name: 'Galamoth', status: 'INACTIVE'},
    {name: 'Lenore', status: 'INACTIVE'},
  ];

  constructor(private userCounterService: UserCounterService) { }

  setActive(idx: number) {
    let user = this.inactiveUsers[idx];
    user.status = 'ACTIVE'
    this.activeUsers.push(user);
    this.inactiveUsers.splice(idx, 1);
    this.userCounterService.increaseActiveToInactive();
  }
  setInactive(idx: number) {
    let user = this.activeUsers[idx];
    user.status = 'INACTIVE'
    this.inactiveUsers.push(user);
    this.activeUsers.splice(idx, 1);
    this.userCounterService.increaseInactiveToActive();
  }
}
