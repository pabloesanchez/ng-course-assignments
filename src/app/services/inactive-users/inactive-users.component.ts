import {Component, Input} from '@angular/core';
import {UserService} from "../user.service";

@Component({
  selector: 'app-inactive-users',
  templateUrl: './inactive-users.component.html',
  styleUrls: ['./inactive-users.component.css']
})
export class InactiveUsersComponent {
  @Input() users: {name: string, status: string }[]  = [];

  constructor(private userService: UserService) {
  }

  setActive(idx: number) {
    this.userService.setActive(idx);
  }
}
