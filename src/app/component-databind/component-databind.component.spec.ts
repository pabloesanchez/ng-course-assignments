import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentDatabindComponent } from './component-databind.component';

describe('ComponentDatabindComponent', () => {
  let component: ComponentDatabindComponent;
  let fixture: ComponentFixture<ComponentDatabindComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ComponentDatabindComponent]
    });
    fixture = TestBed.createComponent(ComponentDatabindComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
