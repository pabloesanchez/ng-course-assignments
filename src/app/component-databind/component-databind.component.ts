import { Component } from '@angular/core';

@Component({
  selector: 'app-component-databind',
  templateUrl: './component-databind.component.html',
  styleUrls: ['./component-databind.component.css']
})
export class ComponentDatabindComponent {
  numbers: number[] = [];

}
