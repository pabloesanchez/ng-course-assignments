import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent {
  count: number = 1;
  @Input() numbers: number[] = [];
  @Output() valuesEvent = new EventEmitter<number>();

  intervalId: any;

  start() {
    this.intervalId = setInterval(() => {
      this.valuesEvent.emit(this.count)
      this.numbers.push(this.count++);
    }, 1000);
  }

  stop() {
    clearInterval(this.intervalId)
  }
}
