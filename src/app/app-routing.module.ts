import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ComponentsComponent} from "./components/components.component";
import {DatabindComponent} from "./databind/databind.component";
import {DirectivesComponent} from "./directives/directives.component";
import {ComponentDatabindComponent} from "./component-databind/component-databind.component";
import {ServicesComponent} from "./services/services.component";
import {TdFormsComponent} from "./td-forms/td-forms.component";
import {ReactiveFormsComponent} from "./reactive-forms/reactive-forms.component";
import {PipesComponent} from "./pipes/pipes.component";

const routes: Routes = [
  {path: 'components', component: ComponentsComponent},
  {path: 'databind', component: DatabindComponent},
  {path: 'directives', component: DirectivesComponent},
  {path: 'component-databind', component: ComponentDatabindComponent},
  {path: 'services', component: ServicesComponent},
  {path: 'td-forms', component: TdFormsComponent},
  {path: 'reactive-forms', component: ReactiveFormsComponent},
  {path: 'pipes', component: PipesComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
