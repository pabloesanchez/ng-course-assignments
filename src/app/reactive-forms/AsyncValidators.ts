import {AsyncValidatorFn, FormControl, ValidationErrors} from "@angular/forms";

export default class AsyncValidators {

  static forbiddenNames: AsyncValidatorFn = formControl => {
    return new Promise<ValidationErrors | null>((resolve, reject) => {
      setTimeout(() => {
        if ('Test' === formControl.value) {
          resolve({invalidProjectName: true});
        } else {
          resolve(null);
        }
      }, 1000)
    })
  }

  /*static forbiddenNames = ((formControl: FormControl) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if ('Test' === formControl.value) {
          resolve({invalidProjectName: true});
        } else {
          resolve(null);
        }
      }, 1000)
    });
  }) as AsyncValidatorFn;*/
}
