import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import AsyncValidators from "./AsyncValidators";
import {Observable} from "rxjs";

@Component({
  selector: 'app-reactive-forms',
  templateUrl: './reactive-forms.component.html',
  styleUrls: ['./reactive-forms.component.css']
})
export class ReactiveFormsComponent {
  form = new FormGroup({
    name: new FormControl('', Validators.required, /*this.forbiddenNames.bind(this)*/ AsyncValidators.forbiddenNames),
    email: new FormControl('', [Validators.required, Validators.email]),
    status: new FormControl('', Validators.required)
  });


  statuses: { value: string, text: string }[] = [
    {value: 'STABLE', text: 'Stable'},
    {value: 'CRITICAL', text: 'Critical'},
    {value: 'FINISHED', text: 'Finished'},
  ];

  /*forbiddenNames(): Observable<any> | Promise<any> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if ('Test' === this.form.get('name')?.value) {
          resolve({invalidProjectName: true});
        } else {
          resolve(null);
        }
      }, 1000)
    });
  }*/

  submitForm() {
    console.log(this.form);
  }
}
