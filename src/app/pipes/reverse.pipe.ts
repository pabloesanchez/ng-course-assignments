import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'reverse'
})
export class ReversePipe implements PipeTransform {

  transform(value: any, ...args: unknown[]) {
    if (value.length === 0) {
      return value;
    }
    /*let reverseString = '';
    for (const char of value) {
      reverseString = char + reverseString;
    }
    return reverseString;*/
    return value.split('').reverse().join('');
  }

}
