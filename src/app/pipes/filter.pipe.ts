import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'filter',
  pure: false
})
export class FilterPipe implements PipeTransform {

  transform(value: any, filterString: string, propName: string) {
    if (value.length === 0 || filterString === '') {
      return value;
    }
    let filteredValues = [];
    for (const v of value) {
      if (v[propName] === filterString) {
        filteredValues.push(v)
      }
    }
    return filteredValues;
  }

}
