import { Component } from '@angular/core';

@Component({
  selector: 'app-pipes',
  templateUrl: './pipes.component.html',
  styleUrls: ['./pipes.component.css']
})
export class PipesComponent {
  servers: { name: string, instanceType: string, started: Date, status: string; }[] = [
    {
      name: 'Production',
      instanceType: 'medium',
      started: new Date(2023, 8, 6),
      status: 'stable'
    },
    {
      name: 'User Database',
      instanceType: 'large',
      started: new Date(2023, 8, 6),
      status: 'stable'
    },
    {
      name: 'Development Server',
      instanceType: 'small',
      started: new Date(2023, 8, 6),
      status: 'offline'
    },
    {
      name: 'Testing Environment Server',
      instanceType: 'small',
      started: new Date(2023, 8, 6),
      status: 'stable'
    }
  ];
  filterString: string = '';
  appAsyncStatus: Promise<string> = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve('stable')
    }, 800)
  });

  addServer() {
    this.servers.push({
      name: 'New Server',
      instanceType: 'medium',
      started: new Date(2023, 8, 6),
      status: 'stable'
    })
  }
}
