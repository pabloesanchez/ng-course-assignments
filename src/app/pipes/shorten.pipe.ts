import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shorten'
})
export class ShortenPipe implements PipeTransform {

  transform(value: any, maxLength: number): unknown {
    if (value.length === 0) {
      return value;
    }
    return value.substring(0, maxLength) + '...';
  }

}
