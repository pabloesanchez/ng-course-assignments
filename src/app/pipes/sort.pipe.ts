import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sort',
  pure: false
})
export class SortPipe implements PipeTransform {

  transform(value: any, ...args: any[]) {
    if (value.length === 0) {
      return value;
    }
    if (args.length === 0) {
      return value.sort();
    }
    return value.sort((a:any, b:any) => {
      return a[args[0]] < b[args[0]] ? -1 : 1;
    });

  }

}
