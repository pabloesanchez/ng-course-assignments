import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WarningAlertComponent } from './components/warning-alert/warning-alert.component';
import { SuccessAlertComponent } from './components/success-alert/success-alert.component';
import { ComponentsComponent } from './components/components.component';
import { DatabindComponent } from './databind/databind.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { DirectivesComponent } from './directives/directives.component';
import { ComponentDatabindComponent } from './component-databind/component-databind.component';
import { GameControlComponent } from './component-databind/game-control/game-control.component';
import { OddComponent } from './component-databind/odd/odd.component';
import { EvenComponent } from './component-databind/even/even.component';
import { ServicesComponent } from './services/services.component';
import { ActiveUsersComponent } from './services/active-users/active-users.component';
import { InactiveUsersComponent } from './services/inactive-users/inactive-users.component';
import { TdFormsComponent } from './td-forms/td-forms.component';
import { ReactiveFormsComponent } from './reactive-forms/reactive-forms.component';
import { PipesComponent } from './pipes/pipes.component';
import { FilterPipe } from './pipes/filter.pipe';
import { ShortenPipe } from './pipes/shorten.pipe';
import { ReversePipe } from './pipes/reverse.pipe';
import { SortPipe } from './pipes/sort.pipe';

@NgModule({
  declarations: [
    AppComponent,
    WarningAlertComponent,
    SuccessAlertComponent,
    ComponentsComponent,
    DatabindComponent,
    DirectivesComponent,
    ComponentDatabindComponent,
    GameControlComponent,
    OddComponent,
    EvenComponent,
    ServicesComponent,
    ActiveUsersComponent,
    InactiveUsersComponent,
    TdFormsComponent,
    ReactiveFormsComponent,
    PipesComponent,
    FilterPipe,
    ShortenPipe,
    ReversePipe,
    SortPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
