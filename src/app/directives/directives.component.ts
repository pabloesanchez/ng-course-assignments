import { Component } from '@angular/core';

@Component({
  selector: 'app-directives',
  templateUrl: './directives.component.html',
  styleUrls: ['./directives.component.css']
})
export class DirectivesComponent {
  display: boolean = false;
  timestamps: Date[] = [];

  showMessageAndCountClick() {
    this.display = true;
    this.timestamps.push(new Date());
  }
}
