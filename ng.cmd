@ECHO off
GOTO start
:find_dp0
SET dp0=%~dp0
EXIT /b
:start
SETLOCAL
CALL :find_dp0

IF EXIST "%userprofile%\programs\nodejs\app\node.exe" (
  SET "_prog=%userprofile%\programs\nodejs\app\node.exe"
) ELSE (
  SET "_prog=node"
  SET PATHEXT=%PATHEXT:;.JS;=;%
)

endLocal & goto #_undefined_# 2>NUL || title %COMSPEC% & "%_prog%"  "%userprofile%\programs\nodejs\app\node_modules\@angular\cli\bin\ng.js" %*
